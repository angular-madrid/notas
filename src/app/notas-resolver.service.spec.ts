import { TestBed } from '@angular/core/testing';

import { NotasResolverService } from './notas-resolver.service';

describe('NotasResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotasResolverService = TestBed.get(NotasResolverService);
    expect(service).toBeTruthy();
  });
});
