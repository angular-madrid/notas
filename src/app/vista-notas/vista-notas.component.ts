import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { NotasService } from '../notas.service';

@Component({
  selector: 'notas-vista-notas',
  templateUrl: './vista-notas.component.html',
  styleUrls: ['./vista-notas.component.css']
})
export class VistaNotasComponent implements OnInit {

  constructor(private notasService: NotasService, private title: Title) { }

  ngOnInit() {
    this.title.setTitle('Listado de notas');
    this.notasService.cargarNotasAPI();
  }

}
