import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotasModule } from './notas/notas.module';
import { EstadisticasModule } from './estadisticas/estadisticas.module';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { AulaComponent } from './aula/aula.component';
import { VistaNotasComponent } from './vista-notas/vista-notas.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    AulaComponent,
    VistaNotasComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NotasModule,
    EstadisticasModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
