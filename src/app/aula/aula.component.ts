import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'notas-aula',
  templateUrl: './aula.component.html',
  styleUrls: ['./aula.component.css']
})
export class AulaComponent implements OnInit {

  @Input()
  public aula: string;

  constructor() { }

  ngOnInit() {
  }

}
