import { NotaNumber } from './nota-number.type';

export interface Nota {
  id: number;
  aula: string;
  nombre: string;
  apellido: string;
  nota: number;
}
