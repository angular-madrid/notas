import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VistaNotasComponent } from './vista-notas/vista-notas.component';
import { NotasResolverService } from './notas-resolver.service';
import { NuevaNotaComponent } from './nueva-nota/nueva-nota.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {
    path: 'crear-nota/:aula', component: NuevaNotaComponent
  },
  {
    path: '', pathMatch: 'full', component: VistaNotasComponent,
  },
  {
    path: '**', component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
