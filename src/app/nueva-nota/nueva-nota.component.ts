import { Component, OnInit, OnDestroy } from '@angular/core';
import { Nota } from '../datos/nota.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NotasService } from '../notas.service';
import { MensajeroService } from '../mensajero.service';

@Component({
  selector: 'notas-nueva-nota',
  templateUrl: './nueva-nota.component.html',
  styleUrls: ['./nueva-nota.component.css']
})
export class NuevaNotaComponent implements OnInit, OnDestroy {

  public nota: Nota = {
    id: null,
    nombre: null,
    apellido: null,
    nota: null,
    aula: null
  };
  private suscripcionURL: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private notasService: NotasService,
    private mensajero: MensajeroService
  ) { }

  public enviaNota() {
    this.nota.aula = this.nota.aula.toUpperCase();
    this.notasService.crearNota(this.nota);
  }

  ngOnInit() {
    this.suscripcionURL = this.route.paramMap.subscribe(
      params => this.nota.aula = params.get('aula')
    );
    this.mensajero.escuchar('notas').subscribe(
      msj => {
        if (msj.texto === 'creada') {
          this.router.navigate(['/']);
        }
      }
    );
  }

  ngOnDestroy() {
    this.suscripcionURL.unsubscribe();
  }

}
