import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadisticasAulaComponent } from './estadisticas-aula.component';

describe('EstadisticasAulaComponent', () => {
  let component: EstadisticasAulaComponent;
  let fixture: ComponentFixture<EstadisticasAulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadisticasAulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadisticasAulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
