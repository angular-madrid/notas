import { Component, OnInit, Input } from '@angular/core';
import { NotasService } from 'src/app/notas.service';
import { MensajeroService } from 'src/app/mensajero.service';

@Component({
  selector: 'notas-estadisticas-aula',
  templateUrl: './estadisticas-aula.component.html',
  styleUrls: ['./estadisticas-aula.component.css']
})
export class EstadisticasAulaComponent implements OnInit {

  @Input()
  public aula: string;
  public notaMedia: number;
  public notaMaxima: number;
  public aprobados: number;
  public aprobadosPorcentaje: number;
  public suspensos: number;
  public suspensosPorcentaje: number;

  constructor(private notasService: NotasService, private mensajero: MensajeroService) { }

  ngOnInit() {
    this.mensajero.escuchar('notas').subscribe(
      msj => {
        if (msj.texto === 'cargado') {
          this.notaMedia = this.notasService.getNotaMediaAula(this.aula);
        }
      }
    );
  }

}
