import { Injectable } from '@angular/core';
import { Nota } from './datos/nota.interface';
import { Resolve } from '@angular/router';
import { NotasService } from './notas.service';

@Injectable({
  providedIn: 'root'
})
export class NotasResolverService implements Resolve<boolean> {

  resolve() {
    return true;
  }

  constructor(private notasService: NotasService) { }
}
