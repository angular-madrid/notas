import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Nota } from './datos/nota.interface';
import { MensajeroService } from './mensajero.service';

import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class NotasService {

  private notas: Array<Nota> = [];
  private notasFiltradas = {
    a: [],
    b: [],
    c: []
  };

  constructor(private http: HttpClient, private mensajero: MensajeroService) { }

  public cargarNotasAPI() {
    this.http.get<Array<Nota>>(`${environment.urlAPI}notas`).subscribe(
      notas => {
        this.notas = notas;
        this.filtraNotasPorAula('a');
        this.filtraNotasPorAula('b');
        this.filtraNotasPorAula('c');
        this.mensajero.emitir({ tema: 'notas', texto: 'cargado' });
      }
    );
  }

  public getNotas() {
    return this.notas;
  }

  public crearNota(nota: Nota) {
    this.http.post(`${environment.urlAPI}notas`, nota).subscribe(
      nuevaNota => {
        this.notasFiltradas[nota.aula.toLowerCase()].push(nuevaNota);
        this.mensajero.emitir({ tema: 'notas', texto: 'creada' });
      }
    );
  }

  public borrarNotaAPI(id: number, aula: string) {
    this.http.delete(`${environment.urlAPI}notas/${id}`, { observe: 'response' }).subscribe(
      resp => {
        if (resp.ok) {
          this.borrarNota(id, aula);
        }
      }
    );
  }

  private borrarNota(id: number, aula: string) {
    const indice = this.notasFiltradas[aula].findIndex(nota => nota.id === id);
    this.notasFiltradas[aula].splice(indice, 1);
  }

  public filtraNotasPorAula(aula: string) {
    this.notasFiltradas[aula] = this.notas.filter(nota => nota.aula.toUpperCase() === aula.toUpperCase());
  }

  public getNotasAula(aula: string) {
    return this.notasFiltradas[aula];
  }

  public getNotaMediaAula(aula: string) {
    const notas = this.notasFiltradas[aula];
    const sumaNotas = notas.reduce(
      (acc, nota) => {
        console.log(nota.nota, acc);
        return nota.nota + acc;
      }, 0
    );
    return sumaNotas / notas.length;
  }
}
