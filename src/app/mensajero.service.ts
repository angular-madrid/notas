import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

interface Mensaje {
  tema: string;
  texto: string;
}

@Injectable({
  providedIn: 'root'
})
export class MensajeroService {

  private mensajero = new BehaviorSubject<Mensaje>(null);

  constructor() { }

  public emitir(mensaje: Mensaje) {
    this.mensajero.next(mensaje);
  }

  public escuchar(tema?: string) {
    const obs = this.mensajero.asObservable();
    if (tema) {
      return obs.pipe(
        filter(mensaje => mensaje && mensaje.tema === tema)
      );
    } else {
      return obs;
    }
  }
}
