import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NotasComponent } from './notas/notas.component';
import { NuevaNotaComponent } from '../nueva-nota/nueva-nota.component';

@NgModule({
  declarations: [NotasComponent, NuevaNotaComponent],
  imports: [
    FormsModule,
    CommonModule
  ],
  exports: [NotasComponent]
})
export class NotasModule { }
