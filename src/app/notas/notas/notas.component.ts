import { Component, OnInit, Input } from '@angular/core';
import { MensajeroService } from 'src/app/mensajero.service';
import { NotasService } from 'src/app/notas.service';
import { Nota } from 'src/app/datos/nota.interface';

@Component({
  selector: 'notas-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.css']
})
export class NotasComponent implements OnInit {

  @Input()
  public aula: string;
  public notas: Array<Nota>;

  constructor(private mensajero: MensajeroService, private notasService: NotasService) { }

  public borrarNota(id: number, e) {
    e.preventDefault();
    this.notasService.borrarNotaAPI(id, this.aula);
  }

  ngOnInit() {
    this.mensajero.escuchar('notas').subscribe(
      msj => {
        switch (msj.texto) {
          case 'cargado':
            this.notas = this.notasService.getNotasAula(this.aula);
            break;
        }
      }
    );
  }

}
